﻿幕間十一 聖女大人的自白

「吶、威爾就快要十三歲生日了吧？」

「說起來、好像是這樣呢。」（ˊ_ˋ）

「你啊……難道不知道自己的生日嗎…..」(ﾟдﾟ)

某一天的午後、在布萊希萊德邊境伯爵借出的住所中、從埃爾那裡聽說我的生日就要到了。

生日。

一般來說、是家人和朋友一起開心慶祝的日子。

肯定會出現美味的蛋糕和料理。

禮物什麼的也會有。

但這和前世我度過的十年不同。

先不說前世還小的時候、到了大學一個人生活後也只有從女朋友手上收過兩次禮物。

成為社會人之後、通常也不會在意生日了。

在二十五歲的生日更是糟糕。

那天加班到很晚、都快要到隔天了才想起自己的生日、但在工作完後、就算想在便利商店裡買蛋糕也都賣完了、沒辦法只好用奶油布丁代替。

這之後卻被小孩子給纏上了、沒想到還發生了更嚴重的事情。

附身到溫德林身上之後、那個家連慶祝生日的想法都沒有。

在那一帶、長男都會被優待、所以大哥庫爾特的生日、食物多少會變的豪華些。

其他兄弟的生日、幾乎完全無視。

父親大概也不記得我的生日吧。

就算記得、以那個家的經濟狀況想舉辦生日也不可能吧。

畢竟兄弟眾多、肯定也沒有一一慶祝的金錢吧。

因此、我身為溫德林對生日的記憶只有在七歲的時候埃裡希哥哥的一句『生日快樂』、在離開家後也只有埃裡希哥哥每年還是會寄信跟禮物給我。

埃裡希哥哥絕對沒有收到這樣的祝福吧。

說起來也很悲慘。

我的家庭對那沒興趣、也不清楚埃裡希哥哥為什麼是唯一的例外。

不過最近、會收到保羅哥哥跟赫爾穆特哥哥的。

但他們在老家、因為不知道何時會離開家裡、所以都忙著自己的事情、因此就不知道該對年紀還小的我送些什麼。

「確實到了十三歲。那也只是人生必經的路程。」( ´Д`)y─･~~

但今年也許會有、埃莉斯、埃爾、伊娜、露易絲跟埃裡希哥哥們、以及布蘭特家族的祝福吧。

自己開始期待起這個妄想來了。σ(´∀『*)

所以、回答就稍微冷淡了點。

肯定在生日派對當天才不會做作、這就是前世所說的傲嬌阿。

一個男生傲嬌什麼的、完全不需要吧。

「就算不特別要求、還是會有人硬給我祝福喔。」

「是這樣嗎？」

「一大堆的貴族們。」

「什麼啊、那已經夠了啊。」

然而、有什麼企圖存在的貴族們、可是遠遠超出埃爾的想像。

他們、太強悍了。

有著那種頑強、在我前世的公司裡說不定已經成為了一流的業務員。

直屬上司也說了『一宮君、有一點毅力吧』等等的。

「喲、小子」

過來拜訪的布蘭塔庫先生、在那裡向我們打招呼。

「布蘭塔庫先生、今天工作好嗎？」

「我就是來工作的、下周週日、要舉行小子的生日派對。」

「都到了這年紀的小孩？」

基本上、貴族都喜歡舉辦派對。

根據時間、規模來舉辦的情形也有。

雖然說也不是100%的人喜歡派對、但為了舉辦派對平常就開始存錢的貴族也有許多。

舉辦出豪華派對的貴族、會被其他的貴族評價為財務能力優秀。

如果主辦方很多、也是想展現出交友圈廣闊。

人脈、是貴族重要的武器之一。

「就算想不舉辦也不行、因為小子是稀有的屠龍的英雄嘛。」

也就是說、想來送禮物的貴族和商人將會蜂擁而至。

然後、期待著回禮會收到什麼。

話說回來、突然想起了在鄉下還很有精神的奶奶說過『在這世界上、最貴的東西就是免費了！』

「但是沒有辦法在這裡舉辦吧。」

這個家對就瀆冒險者預備校的學生來說是豪華了些、以貴族的眼光看來這個家就顯得狹窄了。

庭院特別的狹窄、我認為是不適合舉辦派對的。

「是在布萊希萊德邊境伯爵府舉辦、小子你只要過來就好了。」

「我知道了。」

「真直接啊。」

「不、如果自己全部都沒準備的話還挺討厭的。」

「反正小子是主角、在宴會裡會很匆忙的。」

看來、難得的生日派對會很忙碌的樣子。

總之要應付眾多的人們、已經做好覺悟了。

「但在布萊希萊德邊境伯爵府舉辦的生日派對裡、想順道認識埃莉斯大人的貴族也很多吧。」

威爾的十三歲生日在布萊希萊德邊境伯爵府舉行。

從布蘭塔庫先生那聽說後、身為威爾的未婚妻埃莉斯冷靜的分析了。

埃莉斯身為中央掌握大權的法衣貴族的女兒、卻是個喜歡做些料理、點心、或著泡茶的好姑娘。但他的血統、還是貨真價實的貴族血統。

威爾也挺是有名的、有必要學習各式各樣關於貴族的禮儀和知識。

但說實話、讓威爾學習禮儀的時間完全沒有吧。

在老家甚至還被說不需要。

他們大概也不知道吧。

因此、在宴會裡需要時常待在埃莉斯的身旁吧。

「但是、伊娜和露易絲也會來吧。」

「這三人是一定的。」

「可是、這麼做的話…….」

又會有人不甘心、想把側室啊、作為女傭的情婦啊硬塞給我的事情發生。

光是想到就覺得很恐怖。

「但埃莉斯已經承認我們了啊。」(露易絲)

「我至少也是貴族的女兒。」(伊娜)

在貴族裡只取一個妻子的人確實很少。

在埃莉斯的老家也是如此、同樣是霍恩海姆紅衣主教的孫子、和哥哥的母親就是不同的。

「其實我也是這樣的。」

「我也是。」

大貴族的臣子、果然也有複數的妻子呢。

我討厭與女孩們糾纏、理由就是一切與繼承有關。

實際上、正室跟妾所生的小孩就算年齡相近、只要妾的小孩比較受到寵愛而雙方關係惡劣的例子也不少見。

「但是、還是別讓三人一起進來吧。」

「說的也是呢。」

「埃莉斯、意外的野心十足？」

嘛、三人的關係沒說不好、但繼續增加小妾的還是很麻煩啊。

這裡就先合作、共同抵禦敵人。

某種意義上、就算是魔物來了也無法打倒她們吧。

「還有事情嗎？」

「是、溫德林大人私人的生日派對。」

(從這裡開始第一人稱好像是伊娜)

沒辦法只能在布萊希萊德邊境伯爵府舉行大型的派對、埃莉斯似乎想舉辦只讓這個家關係者參加的小型派對。

「我來製作蛋糕、料理可以拜託你們兩位嗎？」

「說的也是、那種派對威爾也討厭吧。」

對威爾來說、難得布萊希萊德邊境伯爵為自己舉辦的派對也不能說討厭。

身為貴族的責任、就算在討厭還是要出席、我也十分的瞭解。

「總之、決定就三個人了嗎？」

「就這樣子吧、但是還挺意外的。」

「是這樣嗎？」

「嗯、埃莉斯聽了可不要生氣、被霍恩海姆紅衣主教許配給威爾成為夫婦開始、還沒有經過很久、還沒有那麼喜歡威爾吧。」

「我們也是連認識一年都還不到。」

我和露易絲、跟威爾認識時間也沒那麼長。

埃莉斯認識的時間更是短暫。

就對她是否喜歡威爾這點持有疑問了。

雖然我也不怎麼能說別人。

「是阿、一開始從爺爺那裡聽說也嚇到我了。」

到現在為止、很多人都對埃莉斯求婚的樣子。

不論是法衣、領地貴族、還是伯爵家跟男爵家都有。

其他像侯爵、邊境伯爵、公爵家想當做小妾的也有。

埃莉斯說、他們想要能使用治癒魔法『霍恩海姆的聖女』的名聲吧。

貴族確實都想要那種東西呢。

就像現在、被威爾『屠龍的英雄』名聲吸引過來的貴族也很多。

「但爺爺很快就不繼續說下去、一定是察覺到我不願意了。」

身為貴族的女兒、對老家決定的婚事其實是不能拒絕的。

所以真的非常慶幸。

「但那在溫德林大人討伐古代龍之後就……..」

那是在威爾揭見過陛下、取得準男爵地位之後的事。

霍恩海姆紅衣主教突然告訴我。

『埃莉斯、你的丈夫就決定是鮑邁斯特准男爵了。』

『那位屠龍的英雄大人嗎？』

『那是值得高興的、但屠龍的英雄大人是……』

說一句麻煩的事、霍恩海姆家是子爵的家系。

到現在連公爵家都拒絕的情況下、至少要有男爵的地位才不會有怨言發出。

『關於家系平衡的問題不用擔心、陛下很快就會封他為男爵了。』

『難不成是、帕魯肯尼亞草原？』

今天教會的人、因為軍隊向帕魯肯尼亞草原出動、所以擔任了遠征軍的治療人員。

其他不能使用治癒魔法的祭司大人、也到了現場為戰死者祭弔、這也是從軍神官需要做的事。

我們似乎與龍戰鬥的本隊沒有關係。

但也不由得產生已過去無數次擊退遠征軍的老巨龍為對手真的沒問題嗎、可有著屠龍的英雄也會跟著出征的傳言所以要我別擔心。

此外、王宮首席魔導士的伯父大人、還有布萊希萊德邊境伯爵的王牌、有名的布朗塔庫大人也出動了。

看來王國、真的要認真的攻略帕魯肯尼亞草原了。

『鮑邁斯特准男爵回來後、預定會在教會本部進行高級洗禮、就在那個時候介紹。』

『我知道了。』

說真的、我很在意他到底是怎樣的一個人呢？

從爺爺那裡聽說是出生在做為君主臣子像笨蛋一般存在的貧窮貴族的八男。

那樣的人、打倒了龍成為獨立的貴族家主了。

他的心境會發生了怎麼樣的變化呢？

在帕魯肯尼亞草原作戰進行時、當時我也只能待在設置在本陣旁邊的救護所對負傷者進行治療、並傻傻的想著這些事。

途中、伯父大人們在前線的情報陸續傳過來。

伯父大人、使用擅長的魔法格鬥術封鎖龍的行動。

在這期間屠龍的英雄準備了戰略級魔法、老巨龍就無趣地迎接了最後一刻。

布朗塔庫先生這麼說了『能夠悠閒的只在一旁飄浮、不得不說多虧了鮑邁斯特准男爵呢。』

伯父大人雖然用魔法格鬥術壓制了老巨龍。但那魔力消費量龐大也只能持續數分鐘。

『果然、對魔力消耗的計算還是太天真了、如果全功率發動八分後還不能殺死對手的話、死的就是俺了吧。』

伯父大人還讓過來偵查的士兵傳達了這樣的訊息。

在老巨龍死後、伯父大人繼續在前線討伐剩下的魔物。

軍隊與自願參加的冒險者、與危險魔物的戰鬥還在持續著。

救護所一天就有上百名負傷者被運入。

我也拚命的在治療、但是老祭司說了這已經比當年還要少很多了。

『老朽、聽從了先代國王的命令、進入了帕魯肯尼亞草原的部隊。』

在那時、數千人在老巨龍的吐息下一瞬間就死亡了、倖存下來的士兵也充滿著重傷者能不能活到明天都不知道、魔物的追擊又緊接而來。

救護所可是面臨只能運走對撤退還有幫助的人、無法治療的就只能留在原地如同地獄般的慘況。

『救不了的士兵也只能放在魔物預定會經過的道路上、那些士兵被吞噬也只是時間上的問題了、大家也只能哭泣著把戰友給拋棄了。』

『不能用蘇生魔法來救他們嗎？』

『如果還有那種魔力、可是能使用數十次的治癒魔法啊。』

老祭司用著痛苦的表情說著。

儘管如此、這次並不是沒有出現戰死者。

心臟停止跳動、在數小時內用蘇生魔法還是活著的情況很多、但如果肉體損耗太過嚴重還是救不回來。

復活後、也會因為負傷再度死去罷了。

對心臟停止跳動的人使用治癒魔法是沒有效果的、所以蘇生魔法也並非完美。

本來能使用的人就很少了。

『聖女大人、沒有問題嗎？』

眾多的負傷者、偶爾又出現戰死者。

現場太過於悽慘、有人擔心我而向我搭話。

『沒問題的。』

喪氣的話是不會說的、我給他個勉強的微笑。

我可是霍恩海姆的聖女啊。

「嗯….有點難以形容感想呢。」

聽了埃莉斯的從軍故事、我們不由得說不出話來。

我不再愚蠢地想『待在後方、沒有戰鬥真是太輕鬆了』等等的。

「你們兩位也跟著侯爵的軍隊嗎？」

「完全被當作裝飾品了。」

說什麼『會受傷喔』、『會死喔』、只能在後方當埃裡希先生們的護衛。

戰死者只有借陣的士兵五名、大家都說這已經算少了。

但威爾還是在意、出了很多的慰問金。

「然後、就和溫德林大人見面了。」

我原本以為是個外表嚴肅的人、但看到只是個普通人後不免鬆了一口氣。

然後、初次的約會。

『霍恩海姆的聖女、我明白要和屠龍的英雄談話的、但這麼突然感覺到有點壓力。』

『會有壓力嗎？』

『被英雄直率地稱讚什麼的、心臟承受不住啊。』

「當溫德林大人說出那些話的時候、我第一次覺得邂逅了瞭解自己感受的人呢。」

生來就具有治癒魔法的天賦、並使用那魔法受到了許多人的尊敬。

霍恩海姆紅衣主教也是知道、許多打著餿主意的貴族們、都會想利用可憐的她和霍恩海姆家吧。

於是她也為了回應他們的期待而努力下去了。

埃莉斯是瞭解的、霍恩海姆紅衣主教也不想利用他的孫女、但有不得不這麼做的貴族在。

但是、感到有些壓力也是事實。

「看溫德林大人的樣子、我想他不怎麼把我當聖女看。」

「那個、不怎麼喜歡？」

「不、反而覺得很高興。」

只把我當成治癒魔法的特化型魔法師。

但對那種評價反而很高興。

「但在之前製作料理的時候….」

威爾參加教會定期為孤兒院的孩子們製作點心的時候、他稱讚了埃莉斯的料理手腕。

『吶、埃莉斯很擅長料理呢、看起來十分熟練、我的話、就只是大雜燴般的男子料理。』

『我覺得溫德林大人的料理也很美味。』

『只是靠著調味料罷了。』

『確實、我從來沒看過那種茶色的醬、黑色的液體呢。』

好像是叫做醬油的樣子、那是威爾獨自用魔法製造出的產物。

雖然不懂調味料的命名標準、但在帕魯肯尼亞草原的時候、布朗塔庫先生和阿姆斯特朗先生都有著很高的評價。

「我覺得那很像『ショ(肖)』」(某文字君：翻譯不能...google很久也找不到這到底是啥就直接音譯了。)

「『ショ(肖)』是什麼東西啊？」

埃莉斯回答了露易絲的疑問

『ショ(肖』似乎是在王國東部沿海產出的調味料。

將海裡的小魚小蝦加入大量的鹽巴在放進大甕裡發酵。

聞起來有點獨特的氣味、王都裡有進口販賣。

在王都裡、有很多忠實粉絲的樣子。

「聞起來『ショ(肖』的氣味略遜一籌、所以在王都裡販售的話肯定會大受歡迎。」

「(不太妙啊、我還以為有名貴族的女兒不擅長料理呢…..)」

在經驗來說、對方說不定還比較多。

加上、她沏的茶、做的點心也很好吃。

作為同年的女性、浮現了些許敗北感。

「而且、埃莉斯身材很好呢。」థ౪థ

「那很不好意思的………」

露易絲宛如大叔般的發言、讓埃莉斯臉龐染上鮮紅色。(＞///＜)

「男生們都把視線看向……」

就是那對胸部啊。

看見實物之後、反而更加殘酷了。

「我懂得、我也是會把視線往胸部移去。」

「溫德林大人、其實也會這樣的…….」

威爾果然也是男人呢、也會盯著埃莉斯的胸部。

「男生那方面是沒辦法的，我想那不如說是太有魅力了。」

「是那樣子嗎？」

「就是那樣啊、這是一個很現實的問題。」

「伊娜至少有普通的程度我想是沒問題的、我則是危險了、今後還會不會成長都不知道、埃莉斯真好啊、就這樣下去成人後也是勝利組。」ヽ(#`Д´)ﾉ

之後也持續這個話題、從最初決定由我們兩個負責威爾生日派對的料理以外、還聊了下無關的話題。

但是、過的很開心我覺得沒什麼不好。

我想能和埃莉斯成為更好的朋友。
